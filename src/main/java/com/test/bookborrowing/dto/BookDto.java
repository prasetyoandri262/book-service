package com.test.bookborrowing.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDto implements Serializable {
    private static final long serialVersionUID = -6452057510280496294L;

    private String bookId;
    private String title;
    private String author;
    private String publisher;
    private String thick;
    private String status;
    private String loanDate;
    private String dueDate;
    private String returnDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal penalty;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserDto userDto;
}
