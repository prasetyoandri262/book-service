package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetAllDataRequest implements Serializable {
    private static final long serialVersionUID = -707257958883176800L;

    private Integer pageNo;
    private Integer pageSize;
}
