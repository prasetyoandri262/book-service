package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonGetByIdRequest implements Serializable {
    private static final long serialVersionUID = -1402883748722734310L;

    private String id;
}
