package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonDeleteRequest implements Serializable {
    private static final long serialVersionUID = -5606130982148137270L;

    private String id;
}
