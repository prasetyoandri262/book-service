package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateBookReturnedRequest implements Serializable {
    private static final long serialVersionUID = 6325481723063657593L;

    private String idBook;
    private String userId;
    private String returnDate;
}
