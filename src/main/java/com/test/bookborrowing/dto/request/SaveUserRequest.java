package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaveUserRequest implements Serializable {
    private static final long serialVersionUID = 7048420474436080857L;

    private String userId;
    private String name;
    private String nik;
    private String phoneNumber;
    private String address;
    private String email;
}
