package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaveBookRequest implements Serializable {
    private static final long serialVersionUID = -2363842925670248048L;

    private String title;
    private String author;
    private String publisher;
    private String thick;
}
