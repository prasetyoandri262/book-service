package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetAllBookByStatusRequest implements Serializable {
    private static final long serialVersionUID = -4000779740826142973L;

    private Integer pageNo;
    private Integer pageSize;
    private Integer status;
}
