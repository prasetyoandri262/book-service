package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeleteBukuRequest implements Serializable {
    private static final long serialVersionUID = 8517917119844069473L;

    private String idBuku;
}
