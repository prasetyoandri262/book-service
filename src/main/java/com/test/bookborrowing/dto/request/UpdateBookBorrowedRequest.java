package com.test.bookborrowing.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateBookBorrowedRequest implements Serializable {
    private static final long serialVersionUID = 6368919899165205533L;

    private String idBook;
    private String userId;
    private String loanDate;
}
