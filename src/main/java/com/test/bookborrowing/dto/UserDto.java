package com.test.bookborrowing.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {
    private static final long serialVersionUID = 5544950906467102357L;

    private String userId;
    private String name;
    private String nik;
    private String phoneNumber;
    private String address;
    private String email;
}
