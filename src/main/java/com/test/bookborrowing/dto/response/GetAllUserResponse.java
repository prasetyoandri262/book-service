package com.test.bookborrowing.dto.response;

import com.test.bookborrowing.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllUserResponse implements Serializable {
    private static final long serialVersionUID = 8295924144243435527L;

    private List<UserDto> userDtoList;
    private Integer totalPage;
    private Long totalElements;
}
