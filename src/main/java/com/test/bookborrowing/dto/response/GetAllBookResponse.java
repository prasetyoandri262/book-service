package com.test.bookborrowing.dto.response;

import com.test.bookborrowing.dto.BookDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllBookResponse implements Serializable {
    private static final long serialVersionUID = 608768059409253983L;

    private List<BookDto> bookDtoList;
    private Integer totalPage;
    private Long totalElements;
}
