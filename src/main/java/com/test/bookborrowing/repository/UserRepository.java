package com.test.bookborrowing.repository;

import com.test.bookborrowing.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO book_service.user (\"name\", nik, phone_number, address, email, is_deleted, created_by, created_date, modified_by, modified_date) VALUES(?1,?2,?3,?4,?5,false,'SYSTEM',now(),NULL,NULL)", nativeQuery = true)
    Integer insertUser(String name, String nik, String phoneNumber, String address, String email);

    @Modifying
    @Transactional
    @Query(value = "UPDATE book_service.user SET \"name\"=:name, nik=:nik, phone_number=:phoneNumber, address=:address, email=:email, is_deleted=false, modified_by=:id, modified_date=now() WHERE id=:id", nativeQuery = true)
    Integer updateUserById(@Param("id") String id, @Param("name") String name, @Param("nik") String nik, @Param("phoneNumber") String phoneNumber, @Param("address") String address, @Param("email") String email);

    @Query(value = "select u.* from book_service.user u where u.id = ?1 and u.is_deleted = false", nativeQuery = true)
    Optional<User> findById(String id);

    @Query(value = "select u.* from book_service.user u where u.name = ?1 and u.is_deleted = false", nativeQuery = true)
    User findByName(String name);

    @Query(value = "select u.* from book_service.user u where u.nik = ?1 and u.is_deleted = false", nativeQuery = true)
    User findByNik(String nik);

    @Query(value = "select u.* from book_service.user u where u.phone_number = ?1 and u.is_deleted = false", nativeQuery = true)
    User findByPhoneNumber(String phoneNumber);

    @Query(value = "select u.* from book_service.user u where u.is_deleted = false ORDER BY u.id asc",
            countQuery = "SELECT count(1) FROM book_service.user u where u.is_deleted = false",
            nativeQuery = true)
    Page<User> findAllUser(Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM book_service.user u WHERE u.id=?1", nativeQuery = true)
    Integer deleteUserById(String id);
}
