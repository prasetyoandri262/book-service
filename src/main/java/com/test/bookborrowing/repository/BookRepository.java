package com.test.bookborrowing.repository;

import com.test.bookborrowing.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, String> {

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO book_service.book (title, author, publisher, thick, status, is_deleted, user_id, loan_date, due_date, return_date, penalty, created_by, created_date, modified_by, modified_date) VALUES(?1,?2,?3,?4,0, false, NULL, NULL, NULL, NULL, NULL, 'SYSTEM', now(), NULL, NULL)", nativeQuery = true)
    Integer insertBook(String title, String author, String publisher, String thick);

    @Modifying
    @Transactional
    @Query(value = "update book_service.book set status = 1, user_id =:userId, loan_date =:loanDate,due_date = :dueDate, modified_by = :userId, modified_date = now() where id = :id", nativeQuery = true)
    Integer updateBookBorrowedById(@Param("id") String id, @Param("userId") String userId, @Param("loanDate") Date loanDate, @Param("dueDate") Date dueDate);

    @Modifying
    @Transactional
    @Query(value = "update book_service.book set status = :status, user_id =:userId, return_date = :returnDate, penalty = :penalty, modified_by = :userId, modified_date = now() where id = :id", nativeQuery = true)
    Integer updateBookReturnedById(@Param("id") String id, @Param("status") int status, @Param("userId") String userId, @Param("returnDate") Date returnDate, @Param("penalty") BigDecimal penalty);

    @Query(value = "select b.* from book_service.book b where b.id = ?1 and b.is_deleted = false", nativeQuery = true)
    Optional<Book> findById(String id);

    @Query(value = "select b.* from book_service.book b WHERE b.status = ?1 and b.is_deleted = false ORDER BY b.id asc",
            countQuery = "SELECT count(1) FROM book_service.book b WHERE b.status = ?1 and b.is_deleted = false",
            nativeQuery = true)
    Page<Book> findAllByStatus(Integer status, Pageable pageable);

    @Query(value = "select b.* from book_service.book b WHERE b.is_deleted = false ORDER BY b.id asc",
            countQuery = "SELECT count(1) FROM book_service.book b WHERE b.is_deleted = false",
            nativeQuery = true)
    Page<Book> findAllBook(Pageable pageable);

    @Query(value = "select b.* from book_service.book b where b.id = ?1 and b.status = 1 and b.is_deleted = false", nativeQuery = true)
    Book findBookByIdAndStatusBorrowed(String id);

    @Query(value = "select b.* from book_service.book b where b.id = ?1 and b.status = 3 and b.is_deleted = false", nativeQuery = true)
    Book findBookByIdAndStatusReturned(String id);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM book_service.book WHERE id=?1", nativeQuery = true)
    Integer deleteBookById(String id);
}
