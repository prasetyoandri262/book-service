package com.test.bookborrowing.controller;

import com.test.bookborrowing.dto.UserDto;
import com.test.bookborrowing.dto.request.CommonDeleteRequest;
import com.test.bookborrowing.dto.request.CommonGetByIdRequest;
import com.test.bookborrowing.dto.request.GetAllDataRequest;
import com.test.bookborrowing.dto.request.SaveUserRequest;
import com.test.bookborrowing.dto.response.CommonSaveResponse;
import com.test.bookborrowing.dto.response.GetAllUserResponse;
import com.test.bookborrowing.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @ApiOperation("Save User")
    @PostMapping("/user/save")
    public CommonSaveResponse saveUser(@RequestBody SaveUserRequest request) {
        return service.saveUser(request);
    }

    @ApiOperation("Delete User")
    @PostMapping("/user/delete")
    public CommonSaveResponse deleteUserByid(@RequestBody CommonDeleteRequest request) {
        return service.deleteUserById(request);
    }

    @ApiOperation("Get User By Id")
    @PostMapping("/user/get/by-id")
    public UserDto getUserById(@RequestBody CommonGetByIdRequest request) {
        return service.getUserById(request);
    }

    @ApiOperation("Get All User")
    @PostMapping("/user/get-all")
    public GetAllUserResponse getAllUser(@RequestBody GetAllDataRequest request) {
        return service.getAllUser(request);
    }
}
