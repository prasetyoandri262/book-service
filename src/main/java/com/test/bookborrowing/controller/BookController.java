package com.test.bookborrowing.controller;

import com.test.bookborrowing.dto.BookDto;
import com.test.bookborrowing.dto.request.*;
import com.test.bookborrowing.dto.response.CommonSaveResponse;
import com.test.bookborrowing.dto.response.GetAllBookResponse;
import com.test.bookborrowing.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookService service;

    @ApiOperation("Save Book")
    @PostMapping("/book/save")
    public CommonSaveResponse saveBook(@RequestBody SaveBookRequest request) {
        return service.saveBook(request);
    }

    @ApiOperation("Update Book Borrowed")
    @PostMapping("/book/update/borrowed")
    public CommonSaveResponse updateBookBorrowed(@RequestBody UpdateBookBorrowedRequest request) {
        return service.updateBookBorrowed(request);
    }

    @ApiOperation("Update Book Returned")
    @PostMapping("/book/update/returned")
    public CommonSaveResponse updateBookReturned(@RequestBody UpdateBookReturnedRequest request) {
        return service.updateBookReturned(request);
    }

    @ApiOperation("Delete Book By Id")
    @PostMapping("/book/delete/by-id")
    public CommonSaveResponse deleteBookById(@RequestBody CommonDeleteRequest request) {
        return service.deleteBookById(request);
    }

    @ApiOperation("Get Book By Id")
    @PostMapping("/book/get/by-id")
    public BookDto getBookById(@RequestBody CommonGetByIdRequest request) {
        return service.getBookById(request);
    }

    @ApiOperation("Get All Book By Status")
    @PostMapping("/book/get/all/by-status")
    public GetAllBookResponse getAllBookByStatus(@RequestBody GetAllBookByStatusRequest request) {
        return service.getAllBookByStatus(request);
    }
}
