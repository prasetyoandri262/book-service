package com.test.bookborrowing.exception;

public class BadRequestException extends RuntimeException{
    private static final long serialVersionUID = -2613904922805397754L;

    public BadRequestException(String message) {
        super(message);
    }
}
