package com.test.bookborrowing.utils;

import com.test.bookborrowing.constant.BookStatusEnum;
import org.apache.commons.validator.routines.EmailValidator;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class CommonUtil {

    private CommonUtil() {
    }

    private static final String PATTERN2 = "dd MMMM yyyy";

    public static Date getDateByStringFormat(String input) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return formatter.parse(input);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String getDateFormat(Date date) {
        if (ObjectUtils.isEmpty(date))
            return "-";

        DateFormat formatter = new SimpleDateFormat(PATTERN2);
        return formatter.format(date);
    }

    public static Date getDueDateByDate(Date date) {
        if (date == null)
            return null;
        // convert date to localdatetime
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        // plus 30
        localDateTime = localDateTime.plusDays(30);

        // convert LocalDateTime to date
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static BigDecimal getPenalty(Date dueDate, Date returnDate) {
        BigDecimal penalty = new BigDecimal(0);
        BigDecimal penaltyPerDays = new BigDecimal(5000);

        if (returnDate.after(dueDate)) {
            int selisih = Days.daysBetween(new DateTime(dueDate), new DateTime(returnDate)).getDays();
            penalty = penaltyPerDays.multiply(BigDecimal.valueOf(selisih));
        }

        return penalty;
    }

    public static boolean isValidEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static String getBookStatus(Integer status) {
        String result;
        if (status.equals(BookStatusEnum.NOT_YET_BORROWED.ordinal()))
            result = BookStatusEnum.NOT_YET_BORROWED.name().replace("_", " ");
        else if (status.equals(BookStatusEnum.BORROWED.ordinal()))
            result = BookStatusEnum.BORROWED.name();
        else if (status.equals(BookStatusEnum.LATE_RETURNED.ordinal()))
            result = BookStatusEnum.LATE_RETURNED.name().replace("_", " ");
        else result = BookStatusEnum.RETURNED.name();

        return result;
    }

    public static boolean isSuccess(Integer isSuccess) {
        return !ObjectUtils.isEmpty(isSuccess) || !isSuccess.equals(0);
    }
}
