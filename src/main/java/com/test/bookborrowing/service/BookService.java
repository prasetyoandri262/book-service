package com.test.bookborrowing.service;

import com.test.bookborrowing.constant.BookStatusEnum;
import com.test.bookborrowing.constant.Constant;
import com.test.bookborrowing.dto.BookDto;
import com.test.bookborrowing.dto.UserDto;
import com.test.bookborrowing.dto.request.*;
import com.test.bookborrowing.dto.response.CommonSaveResponse;
import com.test.bookborrowing.dto.response.GetAllBookResponse;
import com.test.bookborrowing.entity.Book;
import com.test.bookborrowing.exception.BadRequestException;
import com.test.bookborrowing.exception.DataNotFoundException;
import com.test.bookborrowing.repository.BookRepository;
import com.test.bookborrowing.repository.UserRepository;
import com.test.bookborrowing.utils.CommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookService {

    private final BookRepository bookRepository;

    private final UserRepository userRepository;

    @Transactional(rollbackFor = Exception.class)
    public CommonSaveResponse saveBook(SaveBookRequest request) {
        log.info("save book req : {}", request);

        if (ObjectUtils.isEmpty(request.getTitle()))
            throw new BadRequestException("title is required");

        if (ObjectUtils.isEmpty(request.getAuthor()))
            throw new BadRequestException("author is required");

        if (ObjectUtils.isEmpty(request.getPublisher()))
            throw new BadRequestException("publisher is required");

        if (ObjectUtils.isEmpty(request.getThick()))
            throw new BadRequestException("thick is required");

        Integer isSuccess;
        try {
            isSuccess = bookRepository.insertBook(
                    request.getTitle(),
                    request.getAuthor(),
                    request.getPublisher(),
                    request.getThick()
            );
            log.info(Constant.LOG_SUCCESS, isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public CommonSaveResponse updateBookBorrowed(UpdateBookBorrowedRequest request) {
        log.info("update book borrowed req : {}", request);

        if (ObjectUtils.isEmpty(request.getIdBook()))
            throw new BadRequestException("idBook is required");

        if (ObjectUtils.isEmpty(request.getUserId()))
            throw new BadRequestException("userId is required");

        if (ObjectUtils.isEmpty(request.getLoanDate()))
            throw new BadRequestException("loanDate is required");

        Integer isSuccess;

        try {
            var book = getBookById(request.getIdBook());
            if (ObjectUtils.isEmpty(book))
                throw new DataNotFoundException("ID Book "+request.getIdBook()+Constant.NOT_FOUND);

            Date loanDate = CommonUtil.getDateByStringFormat(request.getLoanDate());

            if (!ObjectUtils.isEmpty(bookRepository.findBookByIdAndStatusBorrowed(request.getIdBook())))
                throw new BadRequestException("ID Book "+request.getIdBook()+" has been borrowed");

            isSuccess = bookRepository.updateBookBorrowedById(
                    book.getId(),
                    request.getUserId(),
                    loanDate,
                    CommonUtil.getDueDateByDate(loanDate)
            );
            log.info(Constant.LOG_SUCCESS, isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public CommonSaveResponse updateBookReturned(UpdateBookReturnedRequest request) {
        log.info("update book late returned req : {}", request);

        if (ObjectUtils.isEmpty(request.getIdBook()))
            throw new BadRequestException("idBook is required");

        if (ObjectUtils.isEmpty(request.getUserId()))
            throw new BadRequestException("userId is required");

        if (ObjectUtils.isEmpty(request.getReturnDate()))
            throw new BadRequestException("returnDate is required");

        Integer isSuccess;

        try {
            var book = getBookById(request.getIdBook());
            if (ObjectUtils.isEmpty(book))
                throw new DataNotFoundException("idBook "+request.getIdBook()+Constant.NOT_FOUND);

            Date returnDate = CommonUtil.getDateByStringFormat(request.getReturnDate());

            if (!ObjectUtils.isEmpty(bookRepository.findBookByIdAndStatusReturned(request.getIdBook())))
                throw new BadRequestException("idBook "+request.getIdBook()+" has been returned");

            int status;
            if (returnDate == null) {
                status = BookStatusEnum.NOT_YET_BORROWED.ordinal();
            } else if (returnDate.after(book.getDueDate())) {
                status = BookStatusEnum.LATE_RETURNED.ordinal();
            } else {
                status = BookStatusEnum.RETURNED.ordinal();
            }

            isSuccess = bookRepository.updateBookReturnedById(
                    book.getId(),
                    status,
                    request.getUserId(),
                    returnDate,
                    CommonUtil.getPenalty(book.getDueDate(), returnDate)
            );
            log.info(Constant.LOG_SUCCESS, isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public CommonSaveResponse deleteBookById(CommonDeleteRequest request) {
        log.info("delete buku by id req : {}", request);

        if (ObjectUtils.isEmpty(request.getId()))
            throw new BadRequestException("id is required");

        Integer isSuccess;
        try {
            var book = getBookById(request.getId());
            if (ObjectUtils.isEmpty(book))
                throw new DataNotFoundException("id book "+request.getId()+Constant.NOT_FOUND);

            isSuccess = bookRepository.deleteBookById(book.getId());
            log.info(Constant.LOG_SUCCESS, isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    public BookDto getBookById(CommonGetByIdRequest request) {
        log.info("get book by id req : {}", request);

        if (ObjectUtils.isEmpty(request.getId()))
            throw new BadRequestException("id is required");

        try {
            var book = getBookById(request.getId());
            if (ObjectUtils.isEmpty(book))
                throw new DataNotFoundException("Data book with id "+request.getId()+Constant.NOT_FOUND);

            UserDto userDto = null;
            if (!ObjectUtils.isEmpty(book.getUserId())) {
                var user = userRepository.findById(book.getUserId()).orElse(null);
                log.info("user : {}", user);
                if (!ObjectUtils.isEmpty(user)) {
                    userDto = UserDto.builder()
                            .userId(user.getId())
                            .name(user.getName())
                            .phoneNumber(user.getPhoneNumber())
                            .nik(user.getNik())
                            .email(user.getEmail())
                            .address(user.getAddress())
                            .build();
                }
            }

            return BookDto.builder()
                    .bookId(book.getId())
                    .title(book.getTitle())
                    .author(book.getAuthor())
                    .publisher(book.getPublisher())
                    .thick(book.getThick())
                    .status(CommonUtil.getBookStatus(book.getStatus()))
                    .loanDate(CommonUtil.getDateFormat(book.getLoanDate()))
                    .dueDate(CommonUtil.getDateFormat(book.getDueDate()))
                    .returnDate(CommonUtil.getDateFormat(book.getReturnDate()))
                    .penalty(book.getPenalty())
                    .userDto(userDto)
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    public GetAllBookResponse getAllBookByStatus(GetAllBookByStatusRequest request) {
        log.info("get all user by status req : {}", request);

        // validation input
        if (ObjectUtils.isEmpty(request.getPageNo()) || request.getPageNo() <= 0)
            throw new BadRequestException("pageNo not valid");
        if (ObjectUtils.isEmpty(request.getPageSize()) || request.getPageSize() <= 0)
            throw new BadRequestException("pageSize not valid");

        try {
            // set paging
            var pageable = PageRequest.of(request.getPageNo()-1, request.getPageSize());
            Page<Book> page;

            if (ObjectUtils.isEmpty(request.getStatus())) {
                // get all buku by paging
                page = bookRepository.findAllBook(pageable);
            } else {
                // get all buku by paging
                page = bookRepository.findAllByStatus(request.getStatus(), pageable);
            }
            log.info("book list : {}", page.toList());
            if (CollectionUtils.isEmpty(page.toList())) {
                log.info(Constant.DATA_NOT_FOUND);
                throw new DataNotFoundException(Constant.DATA_NOT_FOUND);
            }

            List<BookDto> bookDtoList = new ArrayList<>();
            for (Book book : page.toList()) {
                UserDto userDto = null;

                if (!ObjectUtils.isEmpty(book.getUserId())) {
                    var user = userRepository.findById(book.getUserId()).orElse(null);
                    if (!ObjectUtils.isEmpty(user)) {
                        userDto = UserDto.builder()
                                .userId(user.getId())
                                .name(user.getName())
                                .phoneNumber(user.getPhoneNumber())
                                .nik(user.getNik())
                                .email(user.getEmail())
                                .address(user.getAddress())
                                .build();
                    }
                }

                bookDtoList.add(BookDto.builder()
                        .bookId(book.getId())
                        .title(book.getTitle())
                        .author(book.getAuthor())
                        .publisher(book.getPublisher())
                        .thick(book.getThick())
                        .status(CommonUtil.getBookStatus(book.getStatus()))
                        .loanDate(CommonUtil.getDateFormat(book.getLoanDate()))
                        .dueDate(CommonUtil.getDateFormat(book.getDueDate()))
                        .returnDate(CommonUtil.getDateFormat(book.getReturnDate()))
                        .penalty(book.getPenalty())
                        .userDto(userDto)
                        .build());
            }

            return GetAllBookResponse.builder()
                    .bookDtoList(bookDtoList)
                    .totalPage(page.getTotalPages())
                    .totalElements(page.getTotalElements())
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    private Book getBookById(String id) {
        var book = bookRepository.findById(id).orElse(null);
        log.info("book : {}", book);
        return book;
    }
}
