package com.test.bookborrowing.service;

import com.test.bookborrowing.constant.Constant;
import com.test.bookborrowing.dto.UserDto;
import com.test.bookborrowing.dto.request.CommonDeleteRequest;
import com.test.bookborrowing.dto.request.CommonGetByIdRequest;
import com.test.bookborrowing.dto.request.GetAllDataRequest;
import com.test.bookborrowing.dto.request.SaveUserRequest;
import com.test.bookborrowing.dto.response.CommonSaveResponse;
import com.test.bookborrowing.dto.response.GetAllUserResponse;
import com.test.bookborrowing.entity.User;
import com.test.bookborrowing.exception.BadRequestException;
import com.test.bookborrowing.exception.DataNotFoundException;
import com.test.bookborrowing.repository.UserRepository;
import com.test.bookborrowing.utils.CommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    public CommonSaveResponse saveUser(SaveUserRequest request) {
        log.info("save user req : {}", request);

        if (ObjectUtils.isEmpty(request.getName()))
            throw new BadRequestException("name is required");

        if (ObjectUtils.isEmpty(request.getNik()))
            throw new BadRequestException("nik is required");

        if (ObjectUtils.isEmpty(request.getPhoneNumber()))
            throw new BadRequestException("phoneNumber is required");

        if (!CommonUtil.isValidEmail(request.getEmail()))
            throw new BadRequestException("email is not valid");

        if (!ObjectUtils.isEmpty(userRepository.findByName(request.getName())))
            throw new BadRequestException("name is already exists");

        if (!ObjectUtils.isEmpty(userRepository.findByNik(request.getNik())))
            throw new BadRequestException("nik is already exists");

        if (!ObjectUtils.isEmpty(userRepository.findByPhoneNumber(request.getPhoneNumber())))
            throw new BadRequestException("phoneNumber is already exists");

        Integer isSuccess;
        try {
            if (ObjectUtils.isEmpty(request.getUserId())) {
                isSuccess = userRepository.insertUser(
                        request.getName(),
                        request.getNik(),
                        request.getPhoneNumber(),
                        request.getAddress(),
                        request.getEmail()
                );
            } else {
                var user = getUserById(request.getUserId());
                if (ObjectUtils.isEmpty(user))
                    throw new DataNotFoundException("User ID "+request.getUserId()+Constant.NOT_FOUND);

                isSuccess = userRepository.updateUserById(
                        request.getUserId(),
                        request.getName(),
                        request.getNik(),
                        request.getPhoneNumber(),
                        request.getAddress(),
                        request.getEmail()
                );
            }
            log.info("isSuccess : {}", isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    public CommonSaveResponse deleteUserById(CommonDeleteRequest request) {
        log.info("delete user by id : {}", request);

        if (ObjectUtils.isEmpty(request.getId()))
            throw new BadRequestException("id is required");

        Integer isSuccess;
        try {
            var user = getUserById(request.getId());
            if (ObjectUtils.isEmpty(user))
                throw new DataNotFoundException("userId "+request.getId()+Constant.NOT_FOUND);

            isSuccess = userRepository.deleteUserById(user.getId());
            log.info("isSuccess : {}", isSuccess);

            return CommonSaveResponse.builder()
                    .isSuccess(CommonUtil.isSuccess(isSuccess))
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    public UserDto getUserById(CommonGetByIdRequest request) {
        log.info("get user by id : {}", request);

        if (ObjectUtils.isEmpty(request.getId()))
            throw new BadRequestException("id is required");

        try {
            var user = getUserById(request.getId());
            if (ObjectUtils.isEmpty(user))
                throw new DataNotFoundException("data user with id "+request.getId()+Constant.NOT_FOUND);

            return UserDto.builder()
                    .userId(user.getId())
                    .name(user.getName())
                    .nik(user.getNik())
                    .phoneNumber(user.getPhoneNumber())
                    .email(user.getEmail())
                    .address(user.getAddress())
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    public GetAllUserResponse getAllUser(GetAllDataRequest request) {
        log.info("get all user paging req : {}", request);

        // validation input
        if (ObjectUtils.isEmpty(request.getPageNo()) || request.getPageNo() <= 0)
            throw new BadRequestException("pageNo not valid");
        if (ObjectUtils.isEmpty(request.getPageSize()) || request.getPageSize() <= 0)
            throw new BadRequestException("pageSize not valid");

        try {
            // set paging
            var pageable = PageRequest.of(request.getPageNo()-1, request.getPageSize());
            // get all buku by paging
            var page = userRepository.findAllUser(pageable);
            log.info("user list : {}", page.toList());
            if (CollectionUtils.isEmpty(page.toList())) {
                log.info(Constant.DATA_NOT_FOUND);
                throw new DataNotFoundException(Constant.DATA_NOT_FOUND);
            }

            List<UserDto> userDtoList = new ArrayList<>();
            for (User user : page.toList()) {
                userDtoList.add(UserDto.builder()
                        .userId(user.getId())
                        .name(user.getName())
                        .nik(user.getNik())
                        .phoneNumber(user.getPhoneNumber())
                        .email(user.getEmail())
                        .address(user.getAddress())
                        .build());
            }

            return GetAllUserResponse.builder()
                    .userDtoList(userDtoList)
                    .totalPage(page.getTotalPages())
                    .totalElements(page.getTotalElements())
                    .build();
        } catch (BadRequestException | DataNotFoundException e) {
            log.error(Constant.LOG_ERROR, e.getMessage());
            throw e;
        }
    }

    private User getUserById(String id) {
        var user = userRepository.findById(id).orElse(null);
        log.info("user : {}", user);
        return user;
    }
}
