package com.test.bookborrowing.constant;

public class Constant {

    private Constant() {
    }

    public static final String DATA_NOT_FOUND = "Data not found";

    public static final String LOG_ERROR = "error in : {} ";
    public static final String LOG_SUCCESS = "isSuccess : {}";
    public static final String NOT_FOUND = " not found";
}
