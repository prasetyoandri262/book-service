package com.test.bookborrowing.constant;

public enum BookStatusEnum {

    NOT_YET_BORROWED,
    BORROWED,
    LATE_RETURNED,
    RETURNED
}
